import Axios from "axios";
import { makeUseAxios } from "axios-hooks";
const axios = Axios.create({
  baseURL: process.env.NEXT_PUBLIC_API_URL,
});

export default axios;

const favoriteInstance = Axios.create({
  baseURL: process.env.NEXT_PUBLIC_FAVORITE_API_URL,
});

export const useFavoriteAxios = makeUseAxios({
  axios: favoriteInstance,
});
