import Image from "next/image";
import Link from "next/link";
import axios from "../../utils/axios-config";
import Layout from "../../components/Layout";
import Icon from "../../components/Icon";
import Thumbnail from "../../components/Thumbnail";
import ReactHtmlParser from "react-html-parser";
import { useMemo } from "react";

const StoryDetail = ({ storyResponse, comics }) => {
  const [story] = storyResponse;
  const { title, description, modified, thumbnail } = story;

  const renderCharacters = useMemo(() => {
    if (!comics.length) {
      return <p className="Detail-condition">No comics available</p>;
    }

    return comics.map((comic) => {
      const {
        id,
        title,
        thumbnail: { path, extension },
      } = comic;

      return (
        <Thumbnail
          id={id}
          key={id}
          src={`${path}.${extension}`}
          name={title}
          path="comics"
        />
      );
    });
  }, [comics]);

  const renderDescription = useMemo(() => {
    if (!description) return "Not Available";
    return ReactHtmlParser(description);
  }, [description]);

  const renderTitle = useMemo(() => {
    if (!title) return "Not Available";
    return ReactHtmlParser(title);
  }, [title]);

  return (
    <div className="Detail-container">
      <Link href="/stories">
        <span className="Detail-redirect">
          <Icon className="fa-solid fa-circle-chevron-left" />
          Back to Stories
        </span>
      </Link>
      <div className="Detail">
        <div className="Detail-image">
          <Image
            src={
              !thumbnail
                ? "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available.jpg"
                : `${thumbnail.path}.${thumbnail.extension}`
            }
            layout="fill"
            priority
            alt={title}
          />
        </div>

        <div className="Detail-content">
          <h2 className="Detail-name">
            <span>{renderTitle}</span>
          </h2>
          <p className="Detail-label">Description:</p>
          <p className="Detail-text">{renderDescription}</p>
          <p className="Detail-label">Modified:</p>
          <p className="Detail-text">{modified}</p>
        </div>
      </div>
      <div className="Detail-collection">
        <h4 className="Detail-title">Comics from the Story</h4>
        <div className="Thumbnail">{renderCharacters}</div>
      </div>
    </div>
  );
};

export async function getServerSideProps(context) {
  const { id } = context.query;

  try {
    const { data } = await axios.get(`/stories/${id}`);
    const storyResponse = data.data.results;
    const { data: comicsResponse } = await axios.get(`/stories/${id}/comics`);
    const comics = comicsResponse.data.results;
    return { props: { comics, storyResponse } };
  } catch (error) {}
}

StoryDetail.getLayout = (page) => <Layout>{page}</Layout>;

export default StoryDetail;
