import Layout from "../../components/Layout";
import axios from "../../utils/axios-config";
import Card from "../../components/Card";
import Section from "../../components/Section";
import { useCallback, useEffect, useState } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import { v4 as uuidv4 } from "uuid";

const pageLimit = 20;
const offset = 20;

const Stories = ({ stories }) => {
  const [list, setList] = useState(stories);
  const [hasMore, setHasMore] = useState(true);
  const [pages, setPages] = useState(0);

  const getMoreCharacters = useCallback(() => {
    setPages((prev) => prev + 1);
  }, []);

  useEffect(() => {
    if (!pages) return;

    const getStories = async () => {
      let params = { limit: pageLimit, offset: offset * pages };
      const { data } = await axios.get(`/stories`, { params });

      const stories = data.data.results;
      setList((prev) => [...prev, ...stories]);
      setHasMore(list.length <= data.data.total);
    };

    getStories();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pages]);

  const renderStories = useCallback((data) => {
    if (!data.length)
      return <p className="Section-error">No matches have been found 😞...</p>;

    return data.map((story) => {
      const { id, title, thumbnail } = story;

      return (
        <Card
          id={id}
          key={uuidv4()}
          name={title}
          image={
            !thumbnail
              ? "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available.jpg"
              : `${thumbnail.path}.${thumbnail.extension}`
          }
          path="stories"
          category="STORY"
        />
      );
    });
  },[]);

  return (
    <Section title="Marvel Stories">
      <InfiniteScroll
        dataLength={list.length}
        hasMore={hasMore}
        next={getMoreCharacters}
      >
        <div className="Card-container">{renderStories(list)}</div>
      </InfiniteScroll>
    </Section>
  );
};

export async function getServerSideProps() {
  const { data } = await axios.get(`/stories`);
  const stories = data.data.results;

  return { props: { stories } };
}

Stories.getLayout = (page) => <Layout>{page}</Layout>;

export default Stories;
