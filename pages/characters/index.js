import Layout from "../../components/Layout";
import axios, { useFavoriteAxios } from "../../utils/axios-config";
import Card from "../../components/Card";
import Section from "../../components/Section";
import Input from "../../components/Input";
import Button from "../../components/Button";
import { useCallback, useEffect, useState } from "react";
import { useDebounce } from "../../hook/useDebounce";
import AsyncSelect from "react-select/async";
import InfiniteScroll from "react-infinite-scroll-component";
import { useSelector } from "react-redux";
import { v4 as uuidv4 } from "uuid";

const pageLimit = 20;
const offset = 20;

const Characters = ({ characters }) => {
  const logged = useSelector((state) => state.user.isLogged);
  const userToken = useSelector((state) => state.user.token);
  const [list, setList] = useState(characters);
  const [filteredList, setFilteredList] = useState([]);
  const [favoritesList, setFavoritesList] = useState([]);
  const [value, setValue] = useState("");
  const [active, setActive] = useState("Name");
  const [selectValue, setSelectValue] = useState("");
  const [selectedComic, setSelectedComic] = useState(null);
  const [hasMore, setHasMore] = useState(true);
  const [pages, setPages] = useState(0);
  const [, refetch] = useFavoriteAxios(
    {
      url: "/favorites",
      headers: { Authorization: `Bearer ${userToken}` },
    },
    { manual: true }
  );

  const verifyFavorite = useCallback(async (category) => {
    try {
      const { data } = await refetch({
        params: {
          category,
        },
      });
      setFavoritesList(data);
    } catch (error) {
      console.log("Error: ", error);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[]);

  useEffect(() => {
    if (!logged) return;
    verifyFavorite("CHARACTER");
    //eslint-disable-next-line
  }, []);

  const getMoreCharacters = useCallback(() => {
    setPages((prev) => prev + 1);
  }, []);

  const handleChange = useCallback((event) => {
    setValue(event.target.value);
  }, []);

  const handleSelect = useCallback((value) => {
    setSelectValue(value);
  }, []);

  const handleSelection = useCallback((value) => {
    setSelectedComic(value);
  }, []);

  const handleActive = useCallback(
    (category) => () => {
      setActive(category);
      setValue("");
    },
    []
  );

  useEffect(() => {
    if (!pages) return;

    const getCharacters = async () => {
      try {
        let params = { limit: pageLimit, offset: offset * pages };
        const { data } = await axios.get(`/characters`, { params });

        const characters = data.data.results;
        setList((prev) => [...prev, ...characters]);
        setHasMore(list.length <= data.data.total);
      } catch (error) {
        console.log("Error: ", error);
      }
    };

    getCharacters();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pages]);

  useEffect(() => {
    if (selectedComic) {
      const getComicCharacters = async () => {
        try {
          const { data } = await axios.get(
            `/comics/${selectedComic.value}/characters`
          );
          setFilteredList(data.data.results);
        } catch (error) {
          console.log("Error: ", error);
        }
      };
      getComicCharacters();
    }
    setFilteredList([]);
  }, [selectedComic]);

  useDebounce(
    () => {
      const searchCharacter = async () => {
        let params = {};

        if (value) {
          if (active === "Name") {
            params = { nameStartsWith: value };
          }
          try {
            const { data } = await axios.get(`/characters`, {
              params,
            });
            setFilteredList(data.data.results);
          } catch (error) {
            console.log("Error: ", error);
          }
        } else {
          setFilteredList([]);
        }
      };
      searchCharacter();
    },
    800,
    [value]
  );

  const loadOptions = async (selectValue, callback) => {
    let params = {};

    if (selectValue) {
      params = { titleStartsWith: selectValue, limit: 40 };
    }

    try {
      const { data } = await axios.get("/comics", { params });
      const filteredComics = data.data.results;

      callback(
        filteredComics.map((comic) => ({
          label: `${comic.title}`,
          value: `${comic.id}`,
        }))
      );
    } catch (error) {
      console.log("Error", error);
    }
  };

  const renderLists = () => {
    if ((!value || !selectedComic) && !filteredList.length)
      return (
        <InfiniteScroll
          dataLength={list.length}
          hasMore={hasMore}
          next={getMoreCharacters}
        >
          <div className="Card-container">{renderCharacters(list)}</div>
        </InfiniteScroll>
      );

    return (
      <div className="Card-container">{renderCharacters(filteredList)}</div>
    );
  };

  const renderCharacters = useCallback(
    (data) => {
      if (!data.length)
        return (
          <p className="Section-error">No matches have been found 😞...</p>
        );

      return data.map((character) => {
        const {
          id,
          name,
          thumbnail: { path, extension },
        } = character;

        return (
          <Card
            id={id}
            key={uuidv4()}
            name={name}
            image={`${path}.${extension}`}
            path="characters"
            list={favoritesList}
            verifyFavorite={verifyFavorite}
            category="CHARACTER"
          />
        );
      });
    },
    [favoritesList, verifyFavorite]
  );

  return (
    <Section title="Marvel Characters">
      <div className="Search">
        <div className="Search-categories">
          <Button
            className={` button ${active === "Name" ? "Active" : ""}`}
            onClick={handleActive("Name")}
          >
            Name
          </Button>
          <Button
            className={` button ${active === "Comic" ? "Active" : ""}`}
            onClick={handleActive("Comic")}
          >
            Comic
          </Button>
        </div>
        <Input
          type="text"
          className={`Input ${active !== "Name" ? "Disabled" : ""}`}
          placeholder="e.g Iron man, Hulk, Thanos..."
          onChange={handleChange}
        />
        <AsyncSelect
          isClearable
          value={selectedComic}
          loadOptions={loadOptions}
          defaultOptions
          placeholder={"--- Search for a comic's characters ---"}
          onInputChange={handleSelect}
          onChange={handleSelection}
          className={`Search-select Custom ${
            active !== "Comic" ? "Disabled" : ""
          }`}
        />
      </div>
      {renderLists()}
    </Section>
  );
};

export async function getServerSideProps() {
  try {
    const { data } = await axios.get(`/characters`);
    const characters = data.data.results;
    return { props: { characters } };
  } catch (error) {
    console.log("Error: ", error);
  }
}

Characters.getLayout = (page) => <Layout>{page}</Layout>;

export default Characters;
