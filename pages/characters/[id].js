import Image from "next/image";
import Link from "next/link";
import axios from "../../utils/axios-config";
import Layout from "../../components/Layout";
import Icon from "../../components/Icon";
import Thumbnail from "../../components/Thumbnail";
import ReactHtmlParser from "react-html-parser";
import { useMemo } from "react";

const CharacterDetail = ({ characterInfo, comics }) => {
  const [character] = characterInfo;

  const {
    name,
    description,
    stories: { items },
    thumbnail: { path, extension },
  } = character;

  const renderStories = useMemo(() =>
    items.map((story) => {
      const { name, resourceURI } = story;
      const storyId = resourceURI.substring(resourceURI.lastIndexOf("/") + 1);

      return (
        <Link key={name} href={`/stories/${storyId}`}>
          <li className="Detail-item">{name}</li>
        </Link>
      );
    }),[items]);

  const renderComics = useMemo(() =>
    comics.map((comic) => {
      const {
        id,
        title,
        thumbnail: { path, extension },
      } = comic;

      return (
        <Thumbnail
          id={id}
          path="comics"
          key={id}
          src={`${path}.${extension}`}
          name={title}
        />
      );
    }),[comics]);

    const renderDescription = useMemo(() =>{
      if(!description) return "Not Available"
      return ReactHtmlParser(description);
    },[description])

  return (
    <div className="Detail-container">
      <Link href="/characters">
        <div className="Detail-redirect">
          <Icon className="fa-solid fa-circle-chevron-left" />
          <span>Back to Characters</span>
        </div>
      </Link>
      <div className="Detail">
        <div className="Detail-image">
          <Image src={`${path}.${extension}`} layout="fill" alt={name} />
        </div>
        <div className="Detail-content">
          <h2 className="Detail-name">
            <span>{name}</span>
          </h2>
          <p className="Detail-label">Description:</p>
          <p className="Detail-text">
            {renderDescription}
          </p>
          <p className="Detail-label">Stories:</p>
          <ul className="Detail-list">{renderStories}</ul>
        </div>
      </div>
      <div className="Detail-collection">
        <h4 className="Detail-title">Comic Appearences</h4>
        <div className="Thumbnail">{renderComics}</div>
      </div>
    </div>
  );
};

export async function getServerSideProps(context) {
  const { id } = context.query;

  try {
    const { data } = await axios.get(`/characters/${id}`);
    const characterInfo = data.data.results;
    const { data: comicsResponse } = await axios.get(`/characters/${id}/comics`);
    const comics = comicsResponse.data.results;
    
    return { props: { characterInfo, comics } };
    
  } catch (error) {
    console.log("Error:",error);
  }

}

CharacterDetail.getLayout = (page) => <Layout>{page}</Layout>;

export default CharacterDetail;
