import HomeCard from "../components/HomeCard";
import Layout from "../components/Layout";

const index = () => {
  return (
    <main className="Home">
      <div className="Home-banner">
        <h1 className="Home-title">
          <span>Marvel App</span>
        </h1>
        <p className="Home-description">
          The perfect website to discover about Marvels greatest collection of
          characters, comics and stories
        </p>
      </div>
      <div className="Home-content">
        <HomeCard title="Characters" text="Learn about Marvels large collection of characters" path="/characters"/>
        <HomeCard title="Comics" text="Learn about Marvels large collection of comics" path="/comics"/>
        <HomeCard title="Stories" text="Learn about Marvels large collection of stories" path="/stories"/>
      </div>
    </main>
  );
};

index.getLayout = (page) => <Layout>{page}</Layout>;

export default index;
