import Layout from "../../components/Layout";
import axios, { useFavoriteAxios } from "../../utils/axios-config";
import Card from "../../components/Card";
import Section from "../../components/Section";
import Input from "../../components/Input";
import Button from "../../components/Button";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useDebounce } from "../../hook/useDebounce";
import CustomSelect from "../../components/CustomSelect";
import InfiniteScroll from "react-infinite-scroll-component";
import { v4 as uuidv4 } from "uuid";
import { useSelector } from "react-redux";

const options = [
  "Comic",
  "Magazine",
  "Trade Paperback",
  "Hardcover",
  "Digest",
  "Graphic Novel",
  "Digital Comic",
  "Infinite Comic",
];
const pageLimit = 20;
const offset = 20;

const Comics = ({ comics }) => {
  const logged = useSelector((state) => state.user.isLogged);
  const userToken = useSelector((state) => state.user.token);
  const [favoritesList, setFavoritesList] = useState([]);
  const [list, setList] = useState(comics);
  const [filteredList, setFilteredList] = useState([]);
  const [value, setValue] = useState("");
  const [active, setActive] = useState("Title");
  const [hasMore, setHasMore] = useState(true);
  const [pages, setPages] = useState(0);
  const [placeholder, setPlaceholder] = useState("e.g X-Men, Avengers... ");
  const [, refetch] = useFavoriteAxios(
    {
      url: "/favorites",
      headers: { Authorization: `Bearer ${userToken}` },
    },
    { manual: true }
  );

  const verifyFavorite = useCallback(async (category) => {
    const { data } = await refetch({
      params: {
        category,
      },
    });
    setFavoritesList(data);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[]);

  useEffect(() => {
    if (!logged) return;
    verifyFavorite("COMIC");
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getMoreComics = useCallback(() => {
    setPages((prev) => prev + 1);
  }, []);

  const handleChange = useCallback((event) => {
    setValue(event.target.value);
  }, []);

  const handleActive = useCallback((category) => {
    if (category === "Title") setPlaceholder("e.g X-Men, Avengers... ");
    if (category === "Issue") setPlaceholder("e.g 1, 54, 102...");
    setActive(category);
    setValue("");
  }, []);

  useEffect(() => {
    if (!pages) return;

    const getComics = async () => {
      let params = { limit: pageLimit, offset: offset * pages };
      const { data } = await axios.get(`/comics`, { params });

      const comics = data.data.results;
      setList((prev) => [...prev, ...comics]);
      setHasMore(list.length <= data.data.total);
    };

    getComics();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pages]);

  useDebounce(
    () => {
      const searchComics = async () => {
        let params = {};
        if (value) {
          if (active === "Title") {
            params = { titleStartsWith: value };
            setPlaceholder("e.g Iron man, Hulk, Thanos...");
          }

          if (active === "Issue") {
            params = { issueNumber: value };
            setPlaceholder("e.g 54, 103...");
          }
          if (active === "Format") {
            params = { format: value };
          }
          const { data } = await axios.get(`/comics`, {
            params,
          });
          setFilteredList(data.data.results);
        } else {
          setFilteredList([]);
        }
      };
      searchComics();
    },
    800,
    [value]
  );

  const renderLists = () => {
    if (!value && !filteredList.length)
      return (
        <InfiniteScroll
          dataLength={list.length}
          hasMore={hasMore}
          next={getMoreComics}
        >
          <div className="Card-container">{renderComics(list)}</div>
        </InfiniteScroll>
      );

    return <div className="Card-container">{renderComics(filteredList)}</div>;
  };

  const renderComics = useCallback(
    (data) => {
      if (!data.length)
        return (
          <p className="Section-error">No matches have been found 😞...</p>
        );

      return data.map((comic) => {
        const {
          id,
          title,
          thumbnail: { path, extension },
        } = comic;
        return (
          <Card
            id={id}
            key={uuidv4()}
            name={title}
            image={`${path}.${extension}`}
            path="comics"
            category="COMIC"
            list={favoritesList}
            verifyFavorite={verifyFavorite}
          />
        );
      });
    },
    [favoritesList, verifyFavorite]
  );

  return (
    <Section title="Marvel Comics">
      <div className="Search">
        <div className="Search-categories">
          <Button
            className={` button ${active === "Title" ? "Active" : ""}`}
            onClick={() => handleActive("Title")}
          >
            Title
          </Button>
          <Button
            className={` button ${active === "Format" ? "Active" : ""}`}
            onClick={() => handleActive("Format")}
          >
            Format
          </Button>
          <Button
            className={` button ${active === "Issue" ? "Active" : ""}`}
            onClick={() => handleActive("Issue")}
          >
            Issue Number
          </Button>
        </div>
        <Input
          type="text"
          className={`Input ${active === "Format" ? "Disabled" : ""}`}
          placeholder={placeholder}
          onChange={handleChange}
        />
        <CustomSelect
          options={options}
          value={value}
          handleChange={handleChange}
          active={active}
        />
      </div>
      {renderLists()}
    </Section>
  );
};

export async function getServerSideProps() {
  try {
    const { data } = await axios.get(`/comics`);
    const comics = data.data.results;

    return { props: { comics } };
  } catch (error) {
    console.log("Error: ", error);
  }
}

Comics.getLayout = (page) => <Layout>{page}</Layout>;

export default Comics;
