import Image from "next/image";
import Link from "next/link";
import axios from "../../utils/axios-config";
import Layout from "../../components/Layout";
import Icon from "../../components/Icon";
import Thumbnail from "../../components/Thumbnail";
import ReactHtmlParser from "react-html-parser";
import { useMemo } from "react";

const ComicDetail = ({ characters, comicInfo }) => {
  const [comic] = comicInfo;

  const {
    prices,
    format,
    thumbnail: { path, extension },
    title,
    description,
    issueNumber,
  } = comic;

  const renderPrices = useMemo(
    () =>
      prices.map((cprice) => {
        const { price, type } = cprice;
        return (
          <p key={type} className="CharacterDetail-text">
            ${price}
          </p>
        );
      }),
    [prices]
  );

  const renderCharacters = useMemo(() => {
    if (!characters.length) {
      return <p className="Detail-condition">No characters available</p>;
    }

    return characters.map((character) => {
      const {
        id,
        name,
        thumbnail: { path, extension },
      } = character;

      return (
        <Thumbnail
          id={id}
          key={id}
          src={`${path}.${extension}`}
          name={name}
          path="characters"
        />
      );
    });
  }, [characters]);

  const renderFormat = useMemo(() => {
    if (!format) return;

    return (
      <div>
        <p className="Detail-label">Format</p>
        <p className="Detail-text">{format}</p>
      </div>
    );
  }, [format]);

  const renderDescription = useMemo(() => {
    if (!description) return "Not Available";
    return ReactHtmlParser(description);
  }, [description]);

  return (
    <div className="Detail-container">
      <Link href="/comics">
        <span className="Detail-redirect">
          <Icon className="fa-solid fa-circle-chevron-left" />
          Back to Comics
        </span>
      </Link>
      <div className="Detail">
        <div className="Detail-image">
          <Image
            src={`${path}.${extension}`}
            layout="fill"
            priority
            alt={title}
          />
        </div>

        <div className="Detail-content">
          <h2 className="Detail-name">
            <span>{title}</span>
          </h2>
          <p className="Detail-label">Description:</p>
          <div className="Detail-text">{renderDescription}</div>
          <div className="Detail-info">
            <div>
              <p className="Detail-label">Issue Number</p>
              <p className="Detail-text">#{issueNumber}</p>
            </div>
            {renderFormat}
            <div>
              <p className="Detail-label">Price</p>
              {renderPrices}
            </div>
          </div>
        </div>
      </div>
      <div className="Detail-collection">
        <h4 className="Detail-title">Characters from the comic</h4>
        <div className="Thumbnail">{renderCharacters}</div>
      </div>
    </div>
  );
};

export async function getServerSideProps(context) {
  const { id } = context.query;

  try {
    const { data } = await axios.get(`/comics/${id}`);
    const comicInfo = data.data.results;
    const { data: charactersResponse } = await axios.get(
      `/comics/${id}/characters`
    );
    const characters = charactersResponse.data.results;
    return { props: { characters, comicInfo } };
  } catch (error) {
    console.log("Error: ", error);
  }
}

ComicDetail.getLayout = (page) => <Layout>{page}</Layout>;

export default ComicDetail;
