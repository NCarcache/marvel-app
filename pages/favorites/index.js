import { Fragment, useEffect, useState, useCallback, useMemo } from "react";
import Section from "../../components/Section";
import Card from "../../components/Card";
import Layout from "../../components/Layout";
import Button from "../../components/Button";
import { useSelector } from "react-redux";
import { useFavoriteAxios } from "../../utils/axios-config";
import axios from "../../utils/axios-config";

const Favorites = () => {
  const [isActive, setIsActive] = useState("CHARACTER");
  const [list, setList] = useState([]);
  const [marvelList, setMarvelList] = useState([]);
  const logged = useSelector((state) => state.user.isLogged);
  const userToken = useSelector((state) => state.user.token);
  const [marvelParam, setMarvelParam] = useState("");

  const [, refetch] = useFavoriteAxios(
    {
      url: "/favorites",
      headers: { Authorization: `Bearer ${userToken}` },
    },
    { manual: true }
  );

  const filterList = (category) => async () => {
    try {
      await refetch({
        params: {
          category,
        },
      });
      setIsActive(category);
    } catch (error) {
      console.log("Error: ", error);
    }
  };

  useEffect(() => {
    getItems();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [list]);

  useEffect(() => {
    fetchFavorites();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isActive]);

  const fetchFavorites = useCallback(async () => {
    try {
      const { data } = await refetch({
        params: {
          category: isActive,
        },
      });
      setList(data);
    } catch (error) {
      console.log("Error: ", error);
    }

    if (isActive === "CHARACTER") setMarvelParam("characters");
    if (isActive === "COMIC") setMarvelParam("comics");
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[isActive]);

  const fetchMarvelItems = async (id) => {
    try {
      const { data } = await axios.get(`/${marvelParam}/${id}`);
      return data;
    } catch (error) {
      console.log("Error: ", error);
    }
  };

  const getItems = async () => {
    const petititons = [];
    list.forEach((item) => {
      const { marvelId } = item;
      petititons.push(fetchMarvelItems(marvelId));
    });

    try {
      let results = await Promise.allSettled(petititons);
      results = results
        .filter((result) => result.status === "fulfilled")
        .map((item) => item.value.data.results[0]);

      setMarvelList(results);
    } catch (error) {
      console.log("Error: ", error);
    }
  };

  const renderCards = useMemo(() => {
    if (!marvelList.length) {
      return <p className="Section-error">You do not have favorites yet 👀</p>;
    }
    return marvelList.map((marvelItem) => {
      const { id, name, title, thumbnail } = marvelItem;

      return (
        <Card
          id={id}
          key={id}
          name={name || title}
          image={
            !thumbnail
              ? "http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available.jpg"
              : `${thumbnail.path}.${thumbnail.extension}`
          }
          path={marvelParam}
          list={list}
          verifyFavorite={fetchFavorites}
        />
      );
    });
  },[marvelList, fetchFavorites, list, marvelParam]);

  const getActive = useCallback(
    (label) => {
      return isActive === label ? "Active" : "";
    },
    [isActive]
  );

  const renderFavorites = () => {
    if (!logged)
      return (
        <p className="Section-error">
          To have a list of your favorite comics and characters, you have to be
          logged in
        </p>
      );

    return (
      <Fragment>
        <div className="Favorite-categories">
          <Button
            className={`button ${getActive("CHARACTER")}`}
            onClick={filterList("CHARACTER")}
          >
            Characters
          </Button>
          <Button
            className={`button ${getActive("COMIC")}`}
            onClick={filterList("COMIC")}
          >
            Comics
          </Button>
        </div>
        <div className="Card-container">{renderCards}</div>
      </Fragment>
    );
  };

  return <Section title="Favorites">{renderFavorites()}</Section>;
};

Favorites.getLayout = (page) => <Layout>{page}</Layout>;

export default Favorites;
