import Image from "next/image";
import Button from "../../components/Button";
import { setIsLogged } from "../../redux/slices/userSlice";
import { useFavoriteAxios } from "../../utils/axios-config";
import { useDispatch } from "react-redux";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import { useRouter } from "next/router";
import FormikInput from "../../components/FormikInput";
import { useState } from "react";

const schema = Yup.object({
  username: Yup.string().required("This field is required"),
  password: Yup.string().required("This field is required"),
});

const Login = () => {
  const [invalid, setInvalid] = useState(false);
  const dispatch = useDispatch();
  const router = useRouter();
  const [, refetch] = useFavoriteAxios("/auth/login", {
    manual: true,
  });

  const handleSubmit = async (values) => {
    const { username, password } = values;

    try {
      const { data } = await refetch({
        method: "POST",
        data: {
          username,
          password,
        },
      });
      dispatch(setIsLogged(data.access_token));
      router.push("/");
      setInvalid(false);
    } catch (error) {
      setInvalid(true);
    }
  };

  return (
    <div className="Login-container">
      <div className="Login">
        <div className="Login-logo">
          <Image
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/Marvel_Logo.svg/2560px-Marvel_Logo.svg.png"
            width={200}
            height={80}
            alt="marvel logo"
          />
        </div>
        <Formik
          initialValues={{ username: "", password: "" }}
          validationSchema={schema}
          onSubmit={handleSubmit}
        >
          <Form className="Login-form">
            <FormikInput type="text" title="username" />
            <FormikInput type="password" title="password" />
            <Button type="submit" className="button">
              Login
            </Button>
          </Form>
        </Formik>
            {invalid && <p className="Login-error">The username or password provided are not correct</p>}
      </div>
      <div className="Login-image"></div>
    </div>
  );
};

export default Login;
