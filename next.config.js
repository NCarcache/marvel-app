/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  images: {
    domains:["upload.wikimedia.org","i.annihil.us"]
  }
}

module.exports = nextConfig
