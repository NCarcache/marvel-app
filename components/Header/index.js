import React from "react";
import Image from "next/image";
import Button from "../Button";

const Header = () => {
  return (
    <header className="Header">
      <Image
        src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/Marvel_Logo.svg/2560px-Marvel_Logo.svg.png"
        width={250}
        height={100}
        alt="Marvel Logo"
      />
      <p className="Header-text">Welcome to Marvel App!</p>
      <div className="Header-filters">
        <Button className="button">Characters</Button>
        <Button className="button">Comics</Button>
      </div>
    </header>
  );
};

export default Header;
