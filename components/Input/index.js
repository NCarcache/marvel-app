const Input = ({placeholder, className, type, onChange}) => {
  return (
    <input type={type} placeholder={placeholder} className={className} onChange={onChange}/>
  )
}

export default Input;