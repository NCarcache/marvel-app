import { ErrorMessage, Field } from "formik";

const FormikInput = ({ title, type }) => {
  return (
    <div className="Login-field">
      <label className="Login-label">{title}</label>
      <Field type={type} name={title} className="Login-input" />
      <ErrorMessage name={title} component="div" className="Login-error" />
    </div>
  );
};

export default FormikInput;
