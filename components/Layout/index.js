import Head from "next/head";
import Navbar from "../Navbar";

const index = ({ children }) => {
  return (
    <>
    <Head>
      <title>Marvel App</title>
      <link rel="icon" href="/favicon.ico"/>
    </Head>
      <div>
        <Navbar />
        {children}
      </div>
    </>
  );
};

export default index;
