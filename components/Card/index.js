import Image from "next/image";
import Link from "next/link";
import { useCallback, useEffect, useMemo, useState } from "react";
import { useSelector } from "react-redux";
import { useFavoriteAxios } from "../../utils/axios-config";
import Icon from "../Icon";
import ReactHtmlParser from "react-html-parser";

const Card = ({ name, image, id, path, list, verifyFavorite, category }) => {
  const logged = useSelector((state) => state.user.isLogged);
  const userToken = useSelector((state) => state.user.token);
  const [favorited, setFavorited] = useState(false);
  const [, refetch] = useFavoriteAxios(
    {
      url: "/favorites",
      headers: { Authorization: `Bearer ${userToken}` },
    },
    { manual: true }
  );

  useEffect(() => {
    if (!list) return;
    const isFavorited = list.some((item) => item.marvelId === id);
    setFavorited(isFavorited);
  }, [list, id]);

  const addToFavorites = useCallback((marvelId, category) => async () => {
    try {
      await refetch({
        method: "POST",
        data: {
          marvelId,
          category,
        },
      });
      verifyFavorite(category);
    } catch (error) {
      console.log("Error:", error);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[]);

  const deleteFavorite = useCallback((marvelId) => async () => {
    try {
      const index = list.findIndex((item) => marvelId === item.marvelId);
      const { id } = list[index];
  
      await refetch({
        url: `/favorites/${id}`,
        method: "DELETE",
      });
      verifyFavorite(category);
      
    } catch (error) {
      console.log("Error: ", error);
      
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[]);

  const renderFavoriteIcon = useMemo(() => {
    if (!logged) return;
    if (category === "STORY") return;

    if (favorited)
      return (
        <Icon
          className="Card-favorite fa-star fa-solid"
          onClick={deleteFavorite(id)}
        />
      );

    return (
      <Icon
        className="Card-favorite fa-star fa-regular"
        onClick={addToFavorites(id, category)}
      />
    );
  }, [logged, category, favorited, addToFavorites, deleteFavorite, id]);

  return (
    <div className="Card">
      {renderFavoriteIcon}
      <div className="Card-image">
        <Image src={image} width={330} height={250} quality={100} alt={name} loading="lazy"/>
      </div>
      <div className="Card-content">
        <p className={`Card-name ${category === "STORY" ? "Clamp" : ""}`}>
          {ReactHtmlParser(name)}
        </p>
        <Link href={`/${path}/${id}`}>
          <span>
            <Icon className="fa-solid fa-circle-chevron-right Card-link" />
          </span>
        </Link>
      </div>
    </div>
  );
};

export default Card;
