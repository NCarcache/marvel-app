import Link from "next/link";
import React from "react";

const HomeCard = ({title, text, path}) => {
  return (
    <div className="HomeCard">
      <h3 className="HomeCard-title">
        <span>{title}</span>
      </h3>
      <p className="HomeCard-text">
        {text}
      </p>
      <Link href={path}>
        <span className="HomeCard-link">View more</span>
      </Link>
    </div>
  );
};

export default HomeCard;
