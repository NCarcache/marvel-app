import Image from "next/image";
import Link from "next/link";
import React from "react";

const Thumbnail = ({id, name, src, path }) => {
  return (
    <Link href={`/${path}/${id}`}>
      <div className="Thumbnail-card">
        <Image src={src} width={160} height={160} alt={name} loading="lazy"/>
        <h6 className="Thumbnail-name">{name}</h6>
      </div>
    </Link>
  );
};

export default Thumbnail;
