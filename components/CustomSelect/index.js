import { useMemo } from "react";

const CustomSelect = ({ options = [], value, handleChange, active }) => {
  const renderOptions = useMemo(
    () =>
      options.map((option) => (
        <option key={option} value={option.toLowerCase()}>
          {option}
        </option>
      )),
    [options]
  );

  return (
    <select
      className={`Search-select ${active !== "Format" ? "Disabled" : ""}`}
      value={value}
      onChange={handleChange}
    >
      <option value="">--- Select a format ---</option>
      {renderOptions}
    </select>
  );
};

export default CustomSelect;
