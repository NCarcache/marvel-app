import React from 'react'

const Section = ({children, title}) => {
  return (
    <div className='Section'>
        <h2 className="Section-title"><span>{title}</span></h2>
        {children}
    </div>
  )
}

export default Section