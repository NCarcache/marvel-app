import Image from "next/image";
import Link from "next/link";
import Button from "../Button";
import { useDispatch, useSelector } from "react-redux";
import { setIsLogged } from "../../redux/slices/userSlice";
import { useCallback, useMemo } from "react";

const Navbar = () => {
  const logged = useSelector((state) => state.user.isLogged);
  const dispatch = useDispatch();

  const handleLogOut = useCallback(() => {
    dispatch(setIsLogged(""));
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[]);

  const renderLogged = useMemo(() => {
    if (!logged)
      return (
        <Link href="/login">
          <div className="Navbar-login">Log In</div>
        </Link>
      );

    return (
      <Button className="Navbar-login" onClick={handleLogOut}>
        Log Out
      </Button>
    );
  },[logged, handleLogOut]);

  return (
    <nav className="Navbar">
      <Link href="/">
        <div className="Navbar-logo">
          <Image
            src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/Marvel_Logo.svg/2560px-Marvel_Logo.svg.png"
            width={90}
            height={40}
            alt="marvel logo"
          />
        </div>
      </Link>
      <ul className="Navbar-list">
        <li>
          <Link href="/characters">
            <span className="Navbar-link">Characters</span>
          </Link>
        </li>
        <li>
          <Link href="/comics">
            <span className="Navbar-link">Comics</span>
          </Link>
        </li>
        <li>
          <Link href="/stories">
            <span className="Navbar-link">Stories</span>
          </Link>
        </li>
        <li>
          <Link href="/favorites">
            <span className="Navbar-link"> Favorites</span>
          </Link>
        </li>
      </ul>

      {renderLogged}
    </nav>
  );
};

export default Navbar;
